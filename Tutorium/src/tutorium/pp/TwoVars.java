package tutorium.pp;

class Xy
{
	public Xy()
	{}
	
	private int x,y;
	
	public synchronized int getX() 
	{
		return this.x;
	}
	
	public synchronized void setX(int x) 
	{
		this.x = x;
	}
	
	public synchronized int getY() 
	{
		return this.y;
	}
	
	public synchronized void setY(int y) 
	{
		this.y = y;
	}
}

class T1 extends Thread
{
	private Xy xy;
	
	public T1(Xy xy) 
	{
		super();
		this.xy = xy;
	}	
	
	public void run()
	{
		this.xy.setX(1);
	}
}

class T2 extends Thread
{
	private Xy xy;
	
	public void run()
	{
		this.xy.setY(2);
	}

	public T2(Xy xy) 
	{
		super();
		this.xy = xy;
	}
	
}

class T3 extends Thread
{
	private Xy xy;
	
	public T3(Xy xy) 
	{
		super();
		this.xy = xy;
	}	
	
	public void run()
	{
		int result = this.xy.getX() + this.xy.getY();
		System.out.println("Ergebnis: " + result);
	}
}

public class TwoVars
{

	public static void main(String[] args)
	{
		Xy xy = new Xy();
		
		T1 t1 = new T1(xy);
		T2 t2 = new T2(xy);
		T3 t3 = new T3(xy);
		
		t1.start();
		t2.start();
		t3.start();	
	}	
}
