package tutorium.pp;

class MyClass implements Runnable
{
	@Override
	public void run() 
	{
		System.out.println("Hallo Welt");
	}	
}

public class Lambda 
{
	Thread t = new Thread(new MyClass());
	
	Thread t1 = new Thread(
			()-> {System.out.println("Hallo Welt");});
	
	Thread t2 = new Thread(
			()-> System.out.println("Hallo Welt"));
	
	public void doWork()
	{
		t.start();
		t1.start();
		t2.start();
	}
}
