package tutorium.pp;

public class OrderedList 
{
	private int[] data;
	private int size;
	
	public OrderedList() 
	{
		this.data = new int[10];
		this.size = 0;
	}

	public synchronized void print()
	{
		for (int i = 0; i < size; i++) 
		{
			System.out.println(data[i] + " ");
		}
		System.out.println();
	}
	
	public synchronized void insert(int x)
	{
		// Vergr��ere bestehendes Array falls n�tig.
		if (this.size == this.data.length)
		{
			int[] newData = new int[2 * this.data.length];
			for (int i = 0; i < this.data.length; i++) 
			{
				newData[i] = this.data[i];
				Thread.yield();
			}
			this.data = newData;
		}
		
		// Finde die Einf�gestelle
		int pos;
		for (pos = 0; pos < this.size; pos++) 
		{
			if (this.data[pos] > x)
			{
				break;
			}
		}
		
		// Verschiebe gr��ere Elemente hinter Einf�gestelle
		for (int i = this.size - 1; i >= pos; i--) 
		{
			this.data[i+1] = this.data[i];
		}
		
		this.data[pos] = x;
		this.size++;
	}

	public static void main(String[] args)
    {
		OrderedList list = new OrderedList();
		ListWriter w1 = new ListWriter(list,500,1);
		ListWriter w2 = new ListWriter(list,1000,501);
		ListWriter w3 = new ListWriter(list,1500,1001);
		w1.start();
		w2.start();
		w3.start();
		try 
		{
			w1.join();
			w2.join();
			w3.join();
		} 
		catch (InterruptedException e) 
		{
		}
		list.print();
    }
}

class ListWriter extends Thread
{
	private OrderedList list;
	private int from, to;
	
	public ListWriter(OrderedList list, int from, int to) 
	{
		this.list = list;
		this.from = from;
		this.to = to;
	}

	@Override
	public void run() 
	{
		if(from <= to)
		{
			for (int i = from; i <= to; i++) 
			{
				this.list.insert(i);
			}
		}
		else
		{
			for (int i = from; i >= to; i--) 
			{
				this.list.insert(i);
			}
		}
	}	
}