package gui.mvc.bit;

public interface IBitModelListener 
{
    public void modelChanged();
}
