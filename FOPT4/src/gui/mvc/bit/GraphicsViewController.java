package gui.mvc.bit;

import java.awt.event.ItemEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JCheckBox;

public class GraphicsViewController implements MouseListener
{
    private IBitModel bm;
    
    public GraphicsViewController(IBitModel bm)
    {
        this.bm = bm;
    }
    
    public void itemStateChanged(ItemEvent evt) 
    {
        JCheckBox sender = (JCheckBox)evt.getSource();
        int index = Integer.valueOf(sender.getName());
        
        if (evt.getStateChange() == ItemEvent.SELECTED)
        {
            this.bm.set(index, true);
        }
        else
        {
            this.bm.set(index, false);
        }
    }
    
    @Override
    public void mouseClicked(MouseEvent e)
    {
        Circle sender = (Circle)e.getSource(); 
        
        if (sender.checkPosition(e.getX(), e.getY()))
        {
            int index = Integer.valueOf(sender.getName());
            this.bm.set(index, !this.bm.get(index));  
        }       
    }

    @Override
    public void mouseEntered(MouseEvent e)
    {
    }

    @Override
    public void mouseExited(MouseEvent e)
    {
    }

    @Override
    public void mousePressed(MouseEvent e)
    {
    }

    @Override
    public void mouseReleased(MouseEvent e)
    { 
    }
}
