package gui.mvc.bit;

import javax.swing.JCheckBox;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class ControlView extends JPanel implements IBitModelListener 
{
    // Attributes
    private IBitModel bm;
    private JCheckBox[] cb;

    // Constructors
    public ControlView(IBitModel bm) 
    {
        super();
        this.bm = bm;
        this.bm.addModelListener(this);
        
        // add all check boxes to panel
        this.cb = new JCheckBox[bm.getLength()];
        for (int i = cb.length - 1; i >= 0; i--)
        {
            String s = Integer.toString(i);
            this.cb[i] = new JCheckBox(s);
            this.cb[i].setName(s);
            this.add(this.cb[i]);
        }
        
        for (JCheckBox box : this.cb)
        {
            BitViewController handler = new BitViewController(this.bm);
            box.addItemListener(handler);
        }
        
        this.modelChanged();
    }

    // Public Methods
    public void modelChanged() 
    {
        for (int i = 0; i < cb.length; i++)
        {
            this.cb[i].setSelected(bm.get(i));
        }
    }  
}
