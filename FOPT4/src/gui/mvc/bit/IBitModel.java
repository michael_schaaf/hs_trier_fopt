package gui.mvc.bit;

public interface IBitModel 
{
    /**
     * Liefert die L�nge des Bitfeldes zur�ck
     * @return L�nge des Bitfeldes
     */
    public int getLength();

    /**
     * Liefert den Wert des Bitfeldes an der Stelle index
     * @param index gew�nschte Stelle des Bitfeldes
     * @return Wert an der Stelle Index
     */
    public boolean get(int index);

    /**
     * Setzt den Wert des Bitfeldes an der Position index
     * und benachrichtige alle listener.
     * @param index gew�nschte Stelle des Bitfeldes
     * @param b true; false
     */
    public void set(int index, boolean b);

    /**
     * Berechnet den Wert des Bitfeldes als long Zahl
     * @return Wert des Bitfeldes als dezimale Zahl
     */
    public long getValue();

    /**
     * Berechnet den Wert des Bitfeldes als 0-1-String
     * @return Wert des Bitfeldes als String
     */
    public String getBinaryString();

    /**
     * Meldet einen listener an.
     * @param l listener
     */
    public void addModelListener(IBitModelListener l);

    /**
     * Meldet einen listener ab.
     * @param l listener
     */
    public void removeModelListener(IBitModelListener l);
}
