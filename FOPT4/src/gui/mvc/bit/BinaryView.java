package gui.mvc.bit;

import javax.swing.JLabel;

@SuppressWarnings("serial")
public class BinaryView extends JLabel implements IBitModelListener 
{
    // Attributes
    private IBitModel bm;

    // Constructors
    public BinaryView(IBitModel bm) 
    {
        super();
        this.bm = bm;
        this.bm.addModelListener(this);
        this.modelChanged();
    }
    
    // Public Methods
    public void modelChanged() 
    {
        this.setText(bm.getBinaryString());
    }  
}
