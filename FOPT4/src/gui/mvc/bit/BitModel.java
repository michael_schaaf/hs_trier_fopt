package gui.mvc.bit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BitModel implements IBitModel 
{
    // Attributes
    private boolean[] bitArray;
    private List<IBitModelListener> observers;

    // Constructors
    public BitModel(int length) 
    {
        if (length < 0)
        {
            throw new IllegalArgumentException("L�nge kleiner als null.");
        }
        
        this.bitArray  = new boolean[length];
        this.observers = new ArrayList<IBitModelListener>();
    }

    // Public Methods
    public int getLength() 
    {
        return this.bitArray.length;
    }

    public boolean get(int index) 
    {
        if (index < 0 || index > this.bitArray.length)
        {
            throw new IllegalArgumentException("Index zu gro� oder kleiner als null.");
        }
        
        return this.bitArray[index];
    }

    public void set(int index, boolean b) 
    {
        if (index < 0 || index > this.bitArray.length)
        {
            throw new IllegalArgumentException("Index zu gro� oder kleiner als null.");
        }
        
        this.bitArray[index] = b;
        
        for (IBitModelListener l : this.observers)
        {
            l.modelChanged();
        }
    }

    public long getValue() 
    {
        long number = 0;
        
        for (int i = this.bitArray.length-1; i >= 0; i--)
        {
            number = number << 1;
            
            if (this.bitArray[i])
            {
                number += 1;
            }    
        }
        
        return number;
    }

    public String getBinaryString() 
    {
        String binString = Long.toBinaryString(this.getValue());
        char[] leadingArray = new char[this.getLength()- binString.length()];
        Arrays.fill(leadingArray, '0');
        String leadingZeros = new String(leadingArray);
        return leadingZeros + binString;
    }

    public void addModelListener(IBitModelListener l) 
    {
        if (l == null)
        {
            throw new IllegalArgumentException("listener ist null.");
        }
        
        if (!this.observers.contains(l))
        {
            this.observers.add(l);
        }  
    }

    public void removeModelListener(IBitModelListener l) 
    {
        if (l == null)
        {
            throw new IllegalArgumentException("listener ist null.");
        }
        
        this.observers.remove(l);
    }
}
