package gui.mvc.bit;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JCheckBox;

public class BitViewController implements ItemListener
{
    private IBitModel bm;
    
    public BitViewController(IBitModel bm)
    {
        this.bm = bm;
    }
    
    public void itemStateChanged(ItemEvent evt) 
    {
        JCheckBox sender = (JCheckBox)evt.getSource();
        int index = Integer.valueOf(sender.getName());
        
        if (evt.getStateChange() == ItemEvent.SELECTED)
        {
            this.bm.set(index, true);
        }
        else
        {
            this.bm.set(index, false);
        }
    }
}
