package gui.mvc.bit;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class Circle extends JPanel
{    
    // Attributes
    private final int diameter = 8;
    private Color color = Color.BLACK;
    private boolean isFilled;
       
    // Constructors
    public Circle(boolean b)
    {
        this.isFilled = b;
    }
    
    
    // Public Methods
    
    public boolean checkPosition(int x, int y)
    {
        int deltaX = (this.diameter / 2) - x;
        int deltaY = (this.diameter / 2) - y;
        
        double delta = Math.sqrt(
                (Math.pow(deltaX, 2.0) 
                        + Math.pow(deltaY, 2.0)));
        
        return (this.diameter / 2) > delta;
    }
    
    public void fillCircle(boolean b)
    {
        this.isFilled = b;
        this.repaint();
    }  
    
    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);

        g.setColor(this.color);
        
        if(this.isFilled)
        {
            g.fillOval(0, 0, this.diameter, this.diameter);
        }
        else
        {
            g.drawOval(0, 0, this.diameter, this.diameter);
        }
    }
} 
