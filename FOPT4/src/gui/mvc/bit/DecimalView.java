package gui.mvc.bit;

import javax.swing.JLabel;

@SuppressWarnings("serial")
public class DecimalView extends JLabel implements IBitModelListener 
{
    // Attributes
    private IBitModel bm;

    // Constructors
    public DecimalView(IBitModel bm) 
    {
        super();
        this.bm = bm;
        this.bm.addModelListener(this);
        this.modelChanged();
    }
    
    // Public Methods
    public void modelChanged() 
    {
        this.setText(Long.toString(bm.getValue()));
    }    
}
