package gui.mvc.bit;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class GraphicsView extends JPanel implements IBitModelListener
{
    // Attributes
    private IBitModel bm;
    private Circle[] circles;
    
    // Constructors
    public GraphicsView(IBitModel bm) 
    {
        this.bm = bm;
        this.bm.addModelListener(this);
        
        this.circles = new Circle[this.bm.getLength()];
        
        for (int i = this.circles.length - 1; i >= 0; i--)
        {
            GraphicsViewController handler = new GraphicsViewController(this.bm);
            
            this.circles[i] = new Circle(false);
            this.circles[i].addMouseListener(handler);
            this.circles[i].setName(Integer.toString(i));
            this.add(this.circles[i]);
        }
        
        this.modelChanged();
    }

    // Public Methods
    @Override
    public void modelChanged()
    {
        for (int i = 0; i < this.circles.length; i++)
        {
            this.circles[i].fillCircle(this.bm.get(i)); 
        }  
    } 
}
