package test;

import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import gui.mvc.bit.*;

@SuppressWarnings("serial")
public class Test extends JFrame
{
    public Test(IBitModel m)
    {
        super("Bit view");
        
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        // mainPanel konfigurieren
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new GridLayout(0,1));
        
        // decimal view konfigurieren
        DecimalView decView = new DecimalView(m);
        decView.setHorizontalAlignment(JLabel.CENTER);
        mainPanel.add(decView);
        
        // binary view konfigurieren
        BinaryView binView = new BinaryView(m);
        binView.setHorizontalAlignment(JLabel.CENTER);
        mainPanel.add(binView);

        // binary view konfigurieren
        ControlView conView = new ControlView(m);
        mainPanel.add(conView);
        
        // graphic view konfigurieren
        GraphicsView graphView = new GraphicsView(m);
        mainPanel.add(graphView);
        
        add(mainPanel);
        setSize(480,240);
        setVisible(true);
    }
    
    public static void main(String[] args)
    {
        IBitModel model = new BitModel(10);
        model.set(0, false);
        model.set(1, true);
        model.set(2, true);
        model.set(3, false);
        model.set(4, true);
        model.set(5, true);
        model.set(6, false);
        model.set(7, true);
        model.set(8, false);
        model.set(9, false);
        
        @SuppressWarnings("unused")
        Test t = new Test(model);
    }
}
