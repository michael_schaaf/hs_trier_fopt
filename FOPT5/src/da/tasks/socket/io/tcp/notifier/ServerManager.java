package da.tasks.socket.io.tcp.notifier;

import java.net.ServerSocket;
import java.net.Socket;

public class ServerManager extends Thread
{
    private ServerData data;
    
    ServerManager(ServerData data, String name)
    {
        this.setName(name);
        this.data = data;
    }    
    
    @Override
    public void run() 
    {
        ServerSocket serverSocket = data.initServer();
        
        if (serverSocket != null)
        {
            while(true)
            {
                // wait for connection then create streams
                try
                {
                    Socket socket = serverSocket.accept();
                    TCPSocket inputSocket = new TCPSocket(socket);
                    
                    int id = data.createServerInputThread(inputSocket);
                    
                    if (data.registerClientById(id, inputSocket))
                    {
                        System.out.println("Registered Client by ID: " + id);
                    }
                }
                catch(Exception e)
                {
                    System.out.println(e.getMessage());
                    e.printStackTrace();
                }
            }
        }  
    }
}
