package da.tasks.socket.io.tcp.notifier;

public class ServerInputListener extends Thread
{
    private ServerData data;
    private int id;
    
    ServerInputListener(int id, ServerData data)
    {
        this.setName("ServerInputThread" + id);
        this.id = id;
        this.data = data;
    }
    
    @Override
    public void run() 
    {
        String input = "";
        ClientInformation info = this.data.getClient(this.id);
        
        System.out.println(this.getName() + " is running");
        
        try 
        {    
            next:
            while (true) 
            {
                // Nachricht vom Client empfangen, to lower und splitten
                input = info.getSocket().receiveLine();
                
                if (input == null)
                {
                    this.data.deRegisterClient(this.id);
                    System.out.println("De registered client with ID " + this.id);
                    break;
                }
                
                String[] inputArgs = input.split("#");
                String address = inputArgs[0];
                String message = inputArgs[1];
                
                if (address.toLowerCase().equals("server"))
                {
                    if (this.data.registerClientByName(this.id, message))
                    {
                        System.out.println(
                                "Registered Client with ID " + id 
                                + " under name: " + message);
                    }
                    continue next;
                }
                
                if (address.toLowerCase().equals("all"))
                {
                    this.data.broadcastClients(info.getName(), message);
                    continue next;
                }
                
                if (!this.data.sendMessage(address, message))
                {
                    System.out.println(
                            "Message to client :" + address + "not sendt!");
                }               
            }
        } 
        catch(Exception e) 
        {
            System.out.println(e.getMessage());
            e.printStackTrace();
        } 
    }
}
