package da.tasks.socket.io.tcp.notifier;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Server
{
    public static void main(String[] args)
    {
        // Teste ob Kommandozeilenargumente ausreichend waren.
        if (args.length != 1)
        {
            System.out.println("Notwendige Kommandozeilenargumente:"
                    + " <Port-Nummer Server>");
            return;
        }
        
        
        // Erstelle Server Data Objekt und einen Management Thread
        ServerData data = new ServerData(Integer.parseInt(args[0]));
        
        ServerManager manager = new ServerManager(data, "ManagementThread");
        manager.start();   
        
        try (BufferedReader inFromUser = new BufferedReader( 
                new InputStreamReader(System.in)))
        {
            // Warte auf Nutzereingabe
            String text = "";
            do
            {
                System.out.println("Bitte Befehl eingeben:");
                text = inFromUser.readLine();
            } 
            while (!text.equals("quit"));
            
            manager.interrupt();
        }
        catch (IOException e)
        {
            System.out.println(e.getMessage());  
        }
        System.out.println("Server beendet");
    }
}
