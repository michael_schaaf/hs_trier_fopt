package da.tasks.socket.io.tcp.notifier;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Client 
{
    public static void main(String[] args) 
    { 
        // Teste ob Kommandozeilenargumente ausreichend waren.
        if (args.length != 2)
        {
            System.out.println("Notwendige Kommandozeilenargumente:"
                    + " <IP-Adresse Server>"
                    + " <Port-Nummer Server>");
            return;
        }

        // Erstelle Socket Connection und starte Input listener
        try(TCPSocket tcpSocket = 
                new TCPSocket(args[0],Integer.parseInt(args[1])))
        {           
            // Erstelle einen Input listener, welcher auf eingehende Nachrichten wartet
            ClientInputListener listener = 
                    new ClientInputListener(tcpSocket, "ClientInputThread");
            listener.start();
            
            // Warte auf Nutzereingabe an der Console
            try (BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in)))
            {
                String userInput = "";
                
                do
                {
                    System.out.println("Bitte Nachricht eingeben:");
                    userInput = inFromUser.readLine();            
                    
                    // Sende validen input string zum Server.
                    if(userInput.split("#").length == 2)
                    {
                        tcpSocket.sendLine(userInput);
                    }
                } 
                while (!userInput.equals("quit"));
            }
            catch (IOException e)
            {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        System.out.println("Client beendet!");
    }
}
