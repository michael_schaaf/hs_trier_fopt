package da.tasks.socket.io.tcp.notifier;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.List;

public class ServerData
{
    private ServerSocket serverSocket;
    private int portNumber;
    private int indexClients = 1;
    private List<ClientInformation> clients;   

    // Constructors
    ServerData(int port)
    {
        this.portNumber = port;
        this.clients = new ArrayList<ClientInformation>();
    }
    
    // Public synchronized Methods
    public synchronized Boolean registerClientById(int id, TCPSocket socket)
    {
        ClientInformation currentClient = null;
        
        // Search client if id is valid
        if (id > 0)
        {    
            currentClient = this.getClient(id);
        }
        
        // If client unknown register client.
        if (currentClient == null)
        {
            currentClient = new ClientInformation(id, socket);
            this.clients.add(currentClient);
            return true;
        }
        
        return false;
    }
    
    public synchronized Boolean registerClientByName(int id, String name)
    {
        // Get client by id to make sure it is found.
        ClientInformation currentClient = this.getClient(id);
        
        try
        {
            // Check if the name is valid
            if (name.toLowerCase().equals("all") || name.toLowerCase().equals("server"))
            {               
                currentClient.getSocket().sendLine("???#name invalid");
                return false; 
            }
        
            ClientInformation foundByName = this.getClient(name);
            
            if (foundByName != null)
            {
                // The name is allready registered
                currentClient.getSocket().sendLine("???#name invalid");
                return false; 
            }
            else 
            { 
                // Register client by name
                currentClient.setName(name);
                this.broadcastClients(this.clientsListToString());
                return true;
            }
        }
        catch (IOException e)
        {
            System.out.println(e.getMessage());
            e.printStackTrace();
            return false;
        }     
    }

    public synchronized void deRegisterClient(int id)
    {
        for (ClientInformation item : this.clients)
        {
            if (item.getId() == id)
            {
                this.clients.remove(item);
                break;
            }
        }

        // Broadcast information about remaining clients
        this.broadcastClients(this.clientsListToString());
    }
      
    public synchronized ServerSocket initServer()
    {
        if (this.serverSocket == null)
        {
            try
            {
                this.serverSocket = new ServerSocket(this.portNumber);
            }
            catch (IOException e)
            {
                System.out.println(e.getMessage()); 
            }
        }
        
        return this.serverSocket;
    }
    
    public synchronized void broadcastClients(String sender, String message)
    {
        //  Create the broadcast message
        String s = "all#" + message;
        
        // Send message to all clients in the list
        for (ClientInformation item : this.clients)
        {
            try
            {
                if (!item.getName().equals(sender))
                {
                    item.getSocket().sendLine(s);
                } 
            }
            catch (IOException e)
            {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
        }
    }
    
    public synchronized void broadcastClients(String message)
    {
        this.broadcastClients("server", message);
    }    
    
    public synchronized int createServerInputThread(TCPSocket tcpSocket)
    {
        int currentIndex = this.indexClients;
        
        // Create a new Server Input listener
        ServerInputListener listener = new ServerInputListener(currentIndex, this);
        listener.start();
        
        // Create regarding clinet information to the input listener
        ClientInformation info = new ClientInformation(currentIndex, tcpSocket);
        if (!this.clients.contains(info))
        {
            this.clients.add(info); 
        }

        // Increment the common index counter
        this.indexClients++;
        
        // Return the last used client index
        return currentIndex;
    } 

    public synchronized String clientsListToString()
    {
        String clientList = "";
        
        for (int i = 0; i < this.clients.size(); i++)
        {
            String name = this.clients.get(i).getName();
            
            if (!name.equals(""))
            {
                if(clientList.length() > 0)
                {
                    clientList += ", ";
                }
                clientList += this.clients.get(i).getName();
            } 
        }

        return "clients: " + clientList;
    }

    public synchronized ClientInformation getClient(int id)
    {       
        for (ClientInformation item : this.clients)
        {
            if (item.getId() == id)
            {
                return item;
            }
        }
        
        return null;
    }
    
    public synchronized ClientInformation getClient(String name)
    {              
        for (ClientInformation item : this.clients)
        {
            if (item.getName().equals(name))
            {
                return item;
            }
        }
        
        return null;
    }
    
    public synchronized Boolean sendMessage(String target, String message)
    {
        try
        {
            ClientInformation receiver = this.getClient(target);
            
            if (receiver == null)
            {
                return false;
            }
            
            receiver.getSocket().sendLine(target + "#" + message);
            return true;
        }
        catch (IOException e)
        {
            System.out.println(e.getMessage());
            e.printStackTrace();
            return false;
        }
    }
}
