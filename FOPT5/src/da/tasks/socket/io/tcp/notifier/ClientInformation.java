package da.tasks.socket.io.tcp.notifier;

public class ClientInformation implements Comparable<ClientInformation>
{
    // ***************************************************************************************
    // Private Attributes
    
    /**
     * Unique id for the client.
     */
    private int id;
    
    /**
     * Name of the client.
     */
    private String name;
    
    /**
     * Regarding socket information of the client.
     */
    private TCPSocket socket;
    
    // ***************************************************************************************
    // Constructors
    
    /**
     * Initialize new instance of the ClientInformation class.
     * @param name The name of the client.
     * @param socket The regarding socket of the client
     */
    ClientInformation(String name, TCPSocket socket)
    {
        this.setId(-1);
        this.setName(name);
        this.setSocket(socket);
    }
    
    /**
     * Initialize new instance of the ClientInformation class.
     * @param id Unique id of the client.
     * @param socket The regarding socket of the client
     */
    ClientInformation(int id, TCPSocket socket)
    {
        this.setName("");
        this.setId(id);
        this.setSocket(socket);
    }

    // ***************************************************************************************
    // Public Methods
    
    public TCPSocket getSocket()
    {
        return socket;
    }

    public void setSocket(TCPSocket socket)
    {
        this.socket = socket;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }
    
    /**
     * Compares an instance of this class with another object.
     * @param o The object to compare with
     * @return true if same type and name or id is equal; otherwise false.
     */
    @Override
    public boolean equals(Object o)
    {
        if(o.getClass().equals(this.getClass()))
        {
            ClientInformation ci = (ClientInformation) o;
            
            if (this.getName().equals(ci.getName())
                    || this.getId() == ci.getId())
            {
                return true; 
            }
        }
        
        return false;
    }

    @Override
    public int compareTo(ClientInformation ci)
    {      
        return ci.name.compareTo(this.name);
    } 
}
