package da.tasks.socket.io.tcp.notifier;

public class ClientInputListener extends Thread
{
    private TCPSocket socket;
    
    ClientInputListener(TCPSocket socket, String name)
    {
        super(name);
        this.socket = socket;
    }
    
    @Override
    public void run() 
    {
        String message = "";

        try 
        {
            while (true) 
            {
                message = socket.receiveLine();
                System.out.println(message);          
            }
        } 
        catch(Exception e) 
        {
            System.out.println(e.getMessage()); 
            System.out.println(message);
        } 
    }
}
