import java.util.Scanner;
import java.util.concurrent.locks.*;

public class Runner 
{
	private int count;
	private Lock lock = new ReentrantLock();
	private Condition cond = lock.newCondition();
	
	private void increment()
	{
		for (int i = 0; i < 10000; i++) 
			this.count++;
	}
	
	public void firstThread() throws InterruptedException
	{
		this.lock.lock();
		
		System.out.println("Waiting...");
		cond.await();
		
		System.out.println("Woken up!");
		
		try {	
			this.increment();
		} 
		finally {
			this.lock.unlock();
		}	
	}
	
	@SuppressWarnings("resource")
	public void secondThread()
	{
		this.lock.lock();
		
		System.out.println("Press return key");
		new Scanner(System.in).nextLine();
		System.out.println("Got return key");
		
		cond.signal();
		
		try {	
			this.increment();
		} 
		finally {
			// Immer notwendig, vor allem nach signal(), da Objekt sonst gesperrt bleibt.
			this.lock.unlock();
		}	
	}
	
	public void finished()
	{
		System.out.println("Count: " + this.count);
	}

}