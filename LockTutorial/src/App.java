public class App 
{

	public static void main(String[] args) throws Exception 
	{
		final Runner runner = new Runner();
		
		Thread t1 = new Thread(
				()-> {try {
					runner.firstThread();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}});
		
		Thread t2 = new Thread(
				()-> runner.secondThread());
		
		t1.start();
		t2.start();
		
		t1.join();
		t2.join();
		
		runner.finished();
	}

}
