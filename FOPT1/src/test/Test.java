package test;

import pp.filelocking.File;

public class Test
{
    public static void main(String[] args)
    {
        File f = new File(100);
        
        f.lock(10, 20);
        f.lock(40, 75);
        System.out.println("locked");
        
        f.unlock(10, 20);
        f.unlock(40, 75);
        System.out.println("unlocked");
        
        f.unlock(25, 35);
    }
}
