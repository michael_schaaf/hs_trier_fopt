package pp.filelocking;

public class Intervall implements Comparable<Intervall>
{
    private int begin;
    private int end;
    
    public Intervall(int begin, int end)
    {
        this.begin = begin;
        this.end = end;
    }

    public int getBegin()
    {
        return begin;
    }

    public void setBegin(int begin)
    {
        this.begin = begin;
    }

    public int getEnd()
    {
        return end;
    }

    public void setEnd(int end)
    {
        this.end = end;
    }
    
    @Override
    public boolean equals(Object o)
    {
        if(o.getClass().equals(this.getClass()))
        {
            Intervall i = (Intervall) o;
            
            if (i.begin == this.begin &&i.end == this.end)
            {
                return true; 
            }
        }
        
        return false;
    }

    @Override
    public int compareTo(Intervall i)
    {
        if (this.equals(i))
        {
            return 0; 
        }
        return (this.begin - i.begin);
    }
    
}
