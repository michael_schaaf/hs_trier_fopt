package pp.filelocking;

import java.util.LinkedList;
import java.util.List;

public class File 
{
    private List<Intervall> lockedRanges;
    
    private int length;
    
    public File (int length)
    {
        if (length < 0)
        {
            throw new IllegalArgumentException("Parameter was negative.");
        }
        
        this.length = length;
        this.lockedRanges = new LinkedList<Intervall>();
    }
    
    public synchronized void lock (int begin, int end)
    {
        // Check parameters
        if (begin < 0 || end < 0)
        {
            throw new IllegalArgumentException("Parameter was negative");
        } 
        
        if (begin > end || end >= this.length)
        {
            throw new IllegalArgumentException("Parameter was to great");
        } 
        
        // Try to add a new locking intervall
        while (this.checkIntervallCollision(begin, end))
        {
            try
            {
                System.out.println ("wait");
                this.wait();
            }
            catch (InterruptedException e){} 
        }
        
        this.lockedRanges.add(new Intervall(begin, end));  
    }
    
    public synchronized void unlock (int begin, int end)
    {
        // Check parameters
        if (begin < 0 || end < 0)
        {
            throw new IllegalArgumentException("Parameter was negative");
        } 
        
        if (begin > end || end >= this.length)
        {
            throw new IllegalArgumentException("Parameter was to great");
        } 
        
        // Check the locking list
        Intervall i = new Intervall(begin, end);
        
        if (this.lockedRanges.isEmpty())
        {
            throw new IllegalArgumentException("List was empty");
        }
        
        // Try to unlock an existing locking intervall
        if (this.lockedRanges.remove(i))
        {
            System.out.println ("Methode unlock: removed lock");
        }
        else
        {
            throw new IllegalArgumentException("Unlock not possible");
        }

        System.out.println ("Methode unlock: notifyAll");
        this.notifyAll();
    }
    
    private boolean checkIntervallCollision(int begin, int end)
    {
        if (!this.lockedRanges.isEmpty())    
        {
            for (Intervall currentRange : lockedRanges)
            {
                if (!((begin < currentRange.getBegin() && end < currentRange.getBegin()) 
                        || (begin > currentRange.getEnd() && end > currentRange.getEnd())))
                {
                    return true;
                }
            }
        }
        return false;
    }
}

