package gui.pizza;

import java.awt.GridLayout;
import java.awt.HeadlessException;
import javax.swing.*;

@SuppressWarnings("serial")
public class Pizza extends JFrame 
{
    // Attribute
    private String[] sizes;
    private double[] basePrices;
    private String[] ingredients;
    private double[] ingredientPrices;
    
    private JCheckBox[] selectedIngredients;
    private JRadioButton[] selectedSizes;
    
    /** @param groessen Bestellbare Pizza Groessen 
     * @param grundPreise Grundpreise der bestellbaren Pizza Groessen  
     * @param zutaten Liste der zusaetzlich waehlbaren Zutaten. 
     * @param zutatenPreise Preise der zusaetzlich waehlbaren Zutaten. 
     * @throws HeadlessException Wenn ein nicht zulaessiges Eingabegeraet genutzt wird
     **/
    public Pizza(String title, String[] sizes, double[] basePrices, String[] ingredients, double[] ingredientPrices)
                    throws HeadlessException 
    {
        super(title);
    
        // �berpr�fen der Eingabeparameter
        if ((sizes.length != basePrices.length)||(ingredients.length != ingredientPrices.length)) 
        {
            throw new IllegalArgumentException();
        }

        this.sizes = sizes;
        this.basePrices = basePrices;
        this.selectedSizes = new JRadioButton[sizes.length];
        
        this.ingredients = ingredients;
        this.ingredientPrices = ingredientPrices;
        this.selectedIngredients = new JCheckBox[ingredients.length + 2];
        
        initView();      
    }

    /**
     * Darstellung des Fensters parametrieren
     */
    private void initView() 
    {
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    
        // Hauptpanel konfigurieren
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new GridLayout(0,1));
    
        // Panel f�r die Zutatenauswahl konfigurieren
        JPanel ingredientPanel = new JPanel();
        ingredientPanel.setLayout(new GridLayout(0,2));
    
        JCheckBox cb1 = new JCheckBox("K\u00e4se");
        cb1.setName("K\u00e4se");
        cb1.setEnabled(false);
        cb1.setSelected(true);
        this.selectedIngredients[0] = cb1;
    
        JCheckBox cb2 = new JCheckBox("Tomaten");
        cb2.setName("Tomaten");
        cb2.setEnabled(false);
        cb2.setSelected(true);
        this.selectedIngredients[1] = cb2;
    
        for (int i = 0; i < this.ingredients.length; i++)
        {
            JCheckBox cb = new JCheckBox(this.ingredients[i]);
            cb.setName(this.ingredients[i]);
            this.selectedIngredients[i+2] = cb;
        }
        
        for (JCheckBox item : this.selectedIngredients)
        {
            ingredientPanel.add(item);
        }
        
        // Panel f�r die Gr��enauswahl konfigurieren
        JPanel sizePanel = new JPanel();
        sizePanel.setLayout(new GridLayout(1,0));
        
        ButtonGroup group = new ButtonGroup();
    
        for (int i = 0; i < this.sizes.length; i++)
        {
            JRadioButton rb = new JRadioButton(this.sizes[i]);
            rb.setName(this.sizes[i]);
            this.selectedSizes[i] = rb;
        }
        this.selectedSizes[0].setSelected(true);
        
        for (JRadioButton item : this.selectedSizes)
        {
            group.add(item);
            sizePanel.add(item);
        }
        
        // Textausgabe konfigurieren
        JTextArea bestelltext = new JTextArea();
        bestelltext.setName("bestelltext");
        bestelltext.setEditable(false);
        bestelltext.setLineWrap(true);
        bestelltext.setWrapStyleWord(true);
        
        // Bestell-Button konfigurieren
        BestellHandler handler = new BestellHandler(bestelltext, this);
        JButton bestellen = new JButton("Bestellen");
        bestellen.setName("bestellen");
        bestellen.addActionListener(handler);
        
        // Frame Ausgabe konfigurieren und einblenden.
        this.add(mainPanel);
        mainPanel.add(ingredientPanel);
        mainPanel.add(sizePanel);
        mainPanel.add(bestellen);
        mainPanel.add(bestelltext);
        this.setSize(520,320);
        this.setVisible(true);
    }

    public String bestellen()
    {
        double price = 0.0;
        String sizeString = "";
        String ingredientString = "K\u00e4se und Tomaten";
        
        for (int i = 0; i < this.selectedSizes.length; i++)
        {
            if (this.selectedSizes[i].isSelected())
            {
                price += this.basePrices[i];
                sizeString = this.sizes[i];
                break;
            }
        }
        
        for (int i = 2; i < this.selectedIngredients.length; i++)
        {
            if (this.selectedIngredients[i].isSelected())
            {
                ingredientString += " und " + this.selectedIngredients[i].getText();
                price += this.ingredientPrices[i-2];
            }
        }
        
        String s = String.format(
                        "Sie haben eine Pizza der Gr��e %s mit %s bestellt. "
                        + "Der Preis betr�gt %s �. Vielen Dank.", sizeString, ingredientString, price);
        
        return s;
    }
    
    public static void main(String[] args)
    {
        new Pizza("Pizza", 
                        new String[]{"klein","normal","gro\u00df"}, 
                        new double[]{5.0, 7.5, 9.0}, 
                        new String[]{"Artischocken","Paprika","Ei","Spinat","Knoblauch","Mais"}, 
                        new double[]{1.0, 1.0, 0.8, 0.8, 0.5, 0.8});
    }
}