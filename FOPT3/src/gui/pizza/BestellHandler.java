package gui.pizza;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextArea;

class BestellHandler implements ActionListener
{
    private JTextArea text;
    private Pizza pizza;
  
    public BestellHandler(JTextArea t, Pizza p)
    {
        this.text = t;
        this.pizza = p;
    }
  
    @Override
    public void actionPerformed(ActionEvent e) 
    {
        this.text.setText(this.pizza.bestellen());
    }  
}
