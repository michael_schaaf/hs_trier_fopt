package Test;

import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.WindowConstants;

public class Test extends JFrame
{
	public Test()
	{
		super("Bit view");
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new GridLayout(0,1));
        
        JSlider slider = new JSlider();
        slider.setMinimum(0);
        slider.setMaximum(200);
        slider.setValue(20);
        
        GoodAnimation animation = new GoodAnimation(slider);
        
        JButton button = new JButton();
        button.addActionListener(animation);
        
        mainPanel.add(button);
        mainPanel.add(slider);
        
        add(mainPanel);
        setSize(480,240);
        setVisible(true);
	}
	
	public static void main(String[] args)
    {
		Test t = new Test();
    }
		
}
