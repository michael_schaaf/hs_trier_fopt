package Test;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JSlider;
import javax.swing.SwingUtilities;

class Ticker extends Thread
{
	private GoodAnimation animator;
	
	public Ticker(GoodAnimation ref)
	{
		this.animator = ref;
		this.start();
	}
	
	public void run()
	{	
		while(!this.animator.IsNewValue())
		{
			try
			{
				SwingUtilities.invokeLater(this.animator);
				Thread.sleep(50);
			}
			catch(InterruptedException e){};
		}
	}
}

public class GoodAnimation implements ActionListener, Runnable
{
	private JSlider slider;
	private int oldValue;
	private int newValue;
	
	public GoodAnimation(JSlider slider)
	{
		this.slider = slider;
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) 
	{
		this.newValue = 100;
		Ticker tick = new Ticker(this);
	}
	
	private void updateSlider()
	{
		this.oldValue = this.slider.getValue();
		int delta;
		
		if(this.oldValue < this.newValue)
		{
			delta = 1;
		}
		else
		{
			delta = -1;
		}
		
		this.oldValue += delta;
		this.slider.setValue(this.oldValue);
	}	

	@Override
	public void run() 
	{
		this.updateSlider();
	}
	
	public boolean IsNewValue()
	{
		return this.slider.getValue() == this.newValue;
	}
}
