package Test;

public class Zahlenschloss
{
	private int[] kombination;
	private int[] aktuell;
	
	public Zahlenschloss(int[] kombi)
	{
	    this.kombination = new int[kombi.length];
	    this.aktuell = new int[kombi.length];
	    
	    for (int i = 0; i < kombi.length; i++)
	    {
	        this.kombination[i] = kombi[i];
	    }
	
	    for (int i = 0; i < kombi.length; i++)
	    {
	        this.aktuell[i] = 0;
	    }
	}

	public int anzahlRaedchen()
	{
	    return this.kombination.length;
	}

	public synchronized int lesen(int radnummer)
	{
	    if(radnummer < 0 || radnummer >= this.aktuell.length)
	    {
	        throw new IllegalArgumentException();
	    }

	    return this.aktuell[radnummer];
	}

	public synchronized void drehen(int radnummer, int zahl)
	{
	    if(radnummer < 0 || radnummer >= this.aktuell.length)
	    {
	        throw new IllegalArgumentException();
	    }

	    if(zahl < 0)
	    {
	        throw new IllegalArgumentException();
	    }

	    this.aktuell[radnummer] = zahl;
	}

	public synchronized void warten()
	{
	    while(!this.pruefe())
	    {
	    	try
	    	{
	    		wait();
	    	}
	    	catch(InterruptedException e){}
	    }
	}
	
	private boolean pruefe()
	{
	    for(int i = 0; i < this.kombination.length; i++)
	    {
	        if(this.aktuell != this.kombination)
	        {
	        	return false;
	        }
	    }
	
	    notifyAll();
	    return true;
	}
}