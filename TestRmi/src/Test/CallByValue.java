package Test;

public class CallByValue 
{
	private int value = 4711;
	
	public static void main(String[] args) 
	{
		// Initialisiere neue Instanz und gebe initialwert aus!
		// "4711"
		CallByValue c = new CallByValue();
		System.out.println(c.value);
		
		// W�re die Parameter�bergabe "CallByReference", w�re die Ausgabe
		// "815"
		c.ChangeValue(c.value);
		System.out.println(c.value);
		// Ausgabe ist aber "4711"
		// Es erfolgte also eine �bergabe "CallByValue"
		
		// Ausgabe des neuen Test Objektes:
		// 4711 Original
		TestObject t1 = new TestObject();
		t1.id = 4711;
		t1.name = "Original";	
		System.out.println(t1.id + " " + t1.name);
		
		// W�re die Parameter�bergabe "CallByReference", 
		// w�re die Ausgabe: 815 Changed
		c.ChangeObject(t1);
		System.out.println(t1.id + " " + t1.name);
		// Ausgabe des Test Objektes ist immer noch:
		// 4711 Original
		
		// !!! In Java gibt es auschlie�lich Call By Value!!!
		// Die einzige Ausnahme bildet RMI.
	}

	private void ChangeValue(int i)
	{
		i = 815;
	}
	
	private void ChangeObject(TestObject t)
	{
		t = new TestObject();
		t.id = 815;
		t.name = "Changed";		
	}
}

class TestObject
{	
	public int id;
	public String name;
}
