package Test;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Hello extends Remote
{
	public abstract String hello(String name) throws RemoteException;
}
