package Test;

public class TestLambda
{
	private int count;
	
	public TestLambda()
	{
		// Innere anonyme Klasse verwendet
		Thread t1 = new Thread(
				new Runnable(){
					public void run(){
						TestLambda.this.count++;
						}
					});
		t1.start();
		
		// Lambda expression verwendet
		/* 
		 * Da Thread einen Konstruktor besitzt, der ein Runnable-
		 * Interface entgegen nimmt,und da Runnable eine Funktionale Schnittstelle ist,
		 * (nur eine abstrakte Methode), ist dem Compiler hier klar, 
		 * was er tun muss. Es wird implizit dasselbe getan, wie bei Thread t1.
		 * Er erzeugt eine neue anonyme Klasse, welche Runnable inmplementiert 
		 * und mit dem Inhalt von Decrease �berschreibt.
		 * !!! Dieser Code ist schneller als die innere anonyme Klasse.
		*/ 
		Thread t2 = new Thread(()-> Decrease());
		t2.start();
		
		/*
		 * Methodenrefferenz verwendet
		 * Hier wird statt der Lambda expression die Methodenreferenz verwendet.
		 * Diese entspricht inhaltlich der Lambda-Expression.
		 */
		Thread t3 = new Thread(this::Increment);
		t3.start();
		
		/* Methodenrefferenz verwendet
		 * Wenn man keinen weiteren Einfluss auf den Thread nehmen m�chte,
		 * reicht dieser Code aus.
		 */
		new Thread(this::Increment).start();
	}
	
	private void Increment()
	{
		this.count++;
	}
	
	protected void Decrease()
	{
		this.count--;
	}
}
