package Test;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.nio.charset.Charset;

public class SimpleTCPClient
{
	public static void main(String[] args) throws IOException
	{
		InetAddress server = InetAddress.getByName(args[0]);		

		try(final Socket socket = new Socket(server,1250))
		{
			BufferedWriter writer = new BufferedWriter(
				new OutputStreamWriter(socket.getOutputStream(), Charset.forName("UTF-8")));
			writer.write("Hallo");
			writer.newLine();
			writer.flush();

			BufferedReader reader = new BufferedReader(
				new InputStreamReader(socket.getInputStream(), Charset.forName("UTF-8")));
			final String input = reader.readLine();
			System.out.println(input);
		}
	}
}