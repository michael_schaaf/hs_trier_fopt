package socket;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class WorkerThread extends Thread{
	
	private final ServerSocket sSocket;
	private final SynchronizedCounter counter;
	
	public WorkerThread(ServerSocket s, SynchronizedCounter c){
		this.sSocket = s;
		this.counter = c;
	}
	
	@Override
	public void run(){
		while(true){
			try(final Socket tcpSocket = sSocket.accept()){
				
				BufferedWriter writer = new BufferedWriter(
						new OutputStreamWriter(tcpSocket.getOutputStream(),"UTF-8"));
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(tcpSocket.getInputStream(),"UTF-8"));
				
				String request = null;
				while((request = reader.readLine()) != null)
				{
					final int answer = this.processRequestAndReturnAnswer(request);
					this.delay(6000);
					writer.write(answer);
					writer.newLine();
					writer.flush();
				}
			} 
			catch (final IOException ioe){ioe.printStackTrace();}		
		}
	}

	private void delay(final long millisToDelay){
		try{
			Thread.sleep(millisToDelay);
		}
		catch (final InterruptedException ie){
			ie.printStackTrace();
		}
	}
	
	private int processRequestAndReturnAnswer(final String request){
		if(request.equals("increment"))
		{
			return this.counter.increment();
		} 
		else if(request.equals("reset"))
		{
			return this.counter.reset();
		}
		else if(request.equals("decrement"))
		{
			return this.counter.decrement();
		}
		else
		{
			return this.counter.getCounter();
		}
	}
}
