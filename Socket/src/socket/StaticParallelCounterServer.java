package socket;

import java.io.IOException;
import java.net.ServerSocket;

public class StaticParallelCounterServer {

	public static final int DEFAULT_PORT = 1250;
	public static final int WORKER_COUNT = 4;
		
	public static void main(String[] args) throws IOException
	{
		final SynchronizedCounter counter = new SynchronizedCounter();
		final ServerSocket sSocket = new ServerSocket(DEFAULT_PORT);
		
		for(int i = 0; i < WORKER_COUNT; i++){
			final WorkerThread worker = new WorkerThread(sSocket, counter);
			worker.start();
		}
	}
}

