package socket;

public class SynchronizedCounter {
	
	private int counter;

	public synchronized int reset(){
		this.counter = 0;
		return this.counter;
	}
	
	public synchronized int increment(){
		this.counter++;
		return this.counter;
	}
	
	public synchronized int decrement(){
		this.counter--;
		return this.counter;
	}
	
	public synchronized int getCounter(){
		return this.counter;
	}
}
