package pp.producerconsumer;

public class Buffer
{
    private int[] data;
    private int head;
    private int tail;
    private int numberOfElements;
    
    public Buffer(int n)
    {
        if(n <= 0)
        {
            throw new IllegalArgumentException("Parameter <= 0");
        }
        
        this.data = new int[n];
        this.head = 0;
        this.tail = 0;
        this.numberOfElements = 0;
    }
    
    /**
     * Put an integer value to the end of the buffer and waits if the buffer is full.
     * @param x Value to put into the buffer.
     */
    public synchronized void put(int x)
    {
        while(this.data.length == this.numberOfElements)
        {
            try
            {
                this.wait();
            }
            catch(InterruptedException e){}
        }

        // Setze letztes Feldelement
        this.data[this.tail] = x;
        this.tail++;
        
        // �berlauf beim letzten Feldindex
        if(this.tail == this.data.length)
        {
            this.tail = 0;
        }
        
        this.numberOfElements++;
        this.notifyAll();
    }
    
    /**
     * Gets the first integer value from the buffer and waits if the buffer is empty.
     * @return Value from the buffer.
     */
    public synchronized int get()
    {
        while(this.numberOfElements == 0)
        {
            try
            {
                this.wait();
            }
            catch(InterruptedException e){}
        }

        // Entnehme das erste Feldelement
        int result = data[this.head];
        this.head++;
        
        // �berlauf beim letzten Feldindex
        if(this.head == this.data.length)
        {
            this.head = 0;
        }
        
        this.numberOfElements--;
        this.notifyAll();
        
        return result;
    }
}