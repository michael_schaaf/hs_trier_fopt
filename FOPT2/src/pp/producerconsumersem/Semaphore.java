package pp.producerconsumersem;

public class Semaphore
{
    private int value;
    
    /**
     * Initialize a new instance of the Semaphore class.
     * @param init Initial value for the semaphore counter.
     */
    public Semaphore(int init)
    {
        if(init < 0)
        {
            throw new IllegalArgumentException("Parameter < 0");
        }
        this.value = init;
    }

    /**
     * Aquire semaphore, decrease semaphore value by one.
     */
    public synchronized void p()
    {
        while(this.value == 0)
        {
            try
            {
                this.wait();
            }
            catch(InterruptedException e){}
        }
        
        this.value--;
    }

    /**
     * Release semaphore, increase semaphore value by one.
     */
    public synchronized void v()
    {
        this.value++;
        this.notify();
    }
}
