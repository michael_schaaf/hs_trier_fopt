package pp.producerconsumersem;

public class Buffer
{
    private Semaphore semPut;
    private Semaphore semGet;
    private Semaphore mutex;
    
    /**
     * Datenfeld
     */
    private int[] data;
    
    /**
     * Zeiger auf das Feldelement, wo eingef�gt werden soll.
     */
    private int head;
    
    /**
     * Zeiger auf das Feldelement, wo entnommen werden soll.
     */
    private int tail;
    
    /**
     * Erstellt eine neue Instanz der Buffer Klasse.
     * @param bufferSize Maximale Anzahl von Werten im Puffer.
     */
    public Buffer(int bufferSize)
    {
        if(bufferSize <= 0)
        {
            throw new IllegalArgumentException("Parameter <= 0");
        }
        
        this.data = new int[bufferSize];
        this.head = 0;
        this.tail = 0;
        
        this.semPut = new Semaphore(bufferSize);
        this.semGet = new Semaphore(0);
        this.mutex = new Semaphore(1);
    }

    /**
     * Put an integer value to the end of the buffer and waits if the buffer is full.
     * @param x Value to put into the buffer.
     */
    public void put(int x)
    {
        this.semPut.p();
        
        this.mutex.p();
        // F�ge Daten hinten an.
        this.data[this.tail] = x;
        this.tail++;
        
        // �berlauf von "tail" beim letzten Feldindex
        if(this.tail == this.data.length)
        {
            this.tail = 0;
        }
        
        this.mutex.v();
        
        this.semGet.v();
    }
    
    /**
     * Gets the first integer value from the buffer and waits if the buffer is empty.
     * @return Value from the buffer.
     */
    public int get()
    { 
        this.semGet.p();
        
        this.mutex.p();
        // Entnehme Daten vorne.
        int result = data[this.head];
        this.head++;
        
        // �berlauf von "head" beim letzten Feldindex
        if(this.head == this.data.length)
        {
            this.head = 0;
        }
        
        this.mutex.v();
        
        this.semPut.v();
        
        return result;
    }
}
