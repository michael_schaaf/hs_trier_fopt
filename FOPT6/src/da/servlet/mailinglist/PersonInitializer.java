package da.servlet.mailinglist;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.*;
import javax.servlet.annotation.*;

@WebListener
public class PersonInitializer implements ServletContextListener
{

    @Override
    public void contextDestroyed(ServletContextEvent event)
    {
    }

    @Override
    public synchronized void contextInitialized(ServletContextEvent event)
    {
        List<Person> persons = new ArrayList<Person>();
        
        ServletContext context = event.getServletContext();
        context.setAttribute("personenliste", persons);
    }
}