package da.servlet.mailinglist;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
@WebServlet("/Show")
public class ShowServlet extends HttpServlet
{ 
    @SuppressWarnings("unchecked")
    protected synchronized void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException
    {
        ServletContext context = this.getServletContext();
        
        List<Person> persons = (List<Person>)context.getAttribute("personenliste");  
        
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<head><title>Show");
        out.println("</title></head>");
        out.println("<body>");
        for (Person p : persons)
        {
            out.println(p.getUsername() + "<br/>");
        }
        out.println("</body>");
        out.println("</html>"); 
    }
}
