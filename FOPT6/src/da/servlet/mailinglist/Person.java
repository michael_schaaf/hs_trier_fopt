package da.servlet.mailinglist;

public class Person
{
    private String username;
    private String password;

    public Person(String username, String password)
    {
        this.username = username;
        this.password = password;
    }

    public String getUsername()
    {
        return this.username;
    }

    public boolean isPasswordValid(final String passwd)
    {
        return this.password.equals(passwd);
    }
    
    @Override
    public boolean equals(Object o)
    {
        if(o.getClass().equals(this.getClass()))
        {
            Person p = (Person)o;
            return p.username.equals(this.username) 
                    && p.password.equals(this.password);
        }
        
        return false;
    }
}
