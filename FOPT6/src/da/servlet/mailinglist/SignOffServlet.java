package da.servlet.mailinglist;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
@WebServlet("/SignOff")
public class SignOffServlet extends HttpServlet
{
    @SuppressWarnings("unchecked")
    protected synchronized void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException
    {
        String name = request.getParameter("benutzername");
        String pass = request.getParameter("passwort");
        if (!name.equals("") && !pass.equals(""))
        {
            Person user = new Person(name, pass);

            ServletContext context = this.getServletContext();     
            List<Person> persons = (List<Person>)context.getAttribute("personenliste");          
            
            persons.remove(user); 
        } 
        this.doGet(request, response);
    }
    
    protected synchronized void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException
    {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<head><title>SignOff");
        out.println("</title></head>");
        out.println("<body>");
        out.println("<form method=\"post\" action=\"SignOff\">");
        out.println("<p>Benutzername: <input type=\"text\" name=\"benutzername\" size=\"40\"/></p>");
        out.println("<p>Passwort: <input type=\"password\" name=\"passwort\" size=\"20\"/></p>");
        out.println("<p>Passwort: <input type=\"submit\" value=\"Abmelden\"</p>");
        out.println("</form>");
        out.println("</body>");
        out.println("</html>"); 
    }
}
