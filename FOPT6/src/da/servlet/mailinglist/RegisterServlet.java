package da.servlet.mailinglist;

import java.io.*;
import java.util.List;

import javax.servlet.*;
import javax.servlet.annotation.*;
import javax.servlet.http.*;

@SuppressWarnings("serial")
@WebServlet("/Register")
public class RegisterServlet extends HttpServlet
{
    @SuppressWarnings("unchecked")
    protected synchronized void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException
    {
        String name = request.getParameter("benutzername");
        String pass = request.getParameter("passwort");
        
        if (!name.equals("") && !pass.equals(""))
        {
            Person user = new Person(name, pass);

            ServletContext context = this.getServletContext();
            List<Person> persons = (List<Person>)context.getAttribute("personenliste");
            
            if (!persons.contains(user))
            {
                persons.add(user);
            }
        } 
        
        this.doGet(request, response);
    }
    
    protected synchronized void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException
    {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<head><title>Register");
        out.println("</title></head>");
        out.println("<body>");
        out.println("<form method=\"post\" action=\"Register\">");
        out.println("<p>Benutzername: <input type=\"text\" name=\"benutzername\" size=\"40\"/></p>");
        out.println("<p>Passwort: <input type=\"password\" name=\"passwort\" size=\"20\"/></p>");
        out.println("<p><input type=\"submit\" value=\"Anmelden\"</p>");
        out.println("</form>");
        out.println("</body>");
        out.println("</html>"); 
    }
}